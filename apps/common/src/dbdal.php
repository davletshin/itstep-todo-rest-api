<?php

declare(strict_types=1);

namespace Common;

use PDO;
use PDOStatement;
use RuntimeException;

use Satori\Micro\Application;

function prepare(PDO $pdo, string $sql, array $values): PDOStatement {
    $stmt = $pdo->prepare($sql);
    $i = 1;
    foreach ($values as $key => $value) {
        $type = PDO::PARAM_STR;
        if (is_null($value)) {
            $type = PDO::PARAM_NULL;
        }
        if (is_bool($value)) {
            $type = PDO::PARAM_BOOL;
        }
        if (is_int($value)) {
            $type = PDO::PARAM_INT;
        }
        if (is_string($key)) {
            $key = ':' . $key;
            $stmt->bindValue($key, $value, $type);
        } else {
            $stmt->bindValue($i++, $value, $type);
        }
    }

    return $stmt;
}

$app->checkItemDbDal = function (Application $app) {
    $pdo = $app->db1;

    return function (string $sql, array $values) use ($pdo): bool {
        $stmt = prepare($pdo, $sql, $values);
        if (!$stmt->execute()) {
            $error = $stmt->errorInfo();
            throw new RuntimeException(sprintf('%s, ANSI:%s', $error[2], $error[0]), $error[1]);
        }
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();

        return $row ? true : false;
    };
};

$app->countItemsDbDal = function (Application $app) {
    $pdo = $app->db1;

    return function (string $sql, array $values, string $fieldName) use ($pdo): int {
        $stmt = prepare($pdo, $sql, $values);
        if (!$stmt->execute()) {
            $error = $stmt->errorInfo();
            throw new RuntimeException(sprintf('%s, ANSI:%s', $error[2], $error[0]), $error[1]);
        }
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();

        return (int) $row[$fieldName];
    };
};

$app->getItemsDbDal = function (Application $app) {
    $pdo = $app->db1;

    return function (string $sql, array $values) use ($pdo): array {
        $stmt = prepare($pdo, $sql, $values);
        if (!$stmt->execute()) {
            $error = $stmt->errorInfo();
            throw new RuntimeException(sprintf('%s, ANSI:%s', $error[2], $error[0]), $error[1]);
        }
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $items[] = $row;
        }
        $stmt->closeCursor();

        return $items ?? [];
    };
};

$app->readItemDbDal = function (Application $app) {
    $pdo = $app->db1;

    return function (string $sql, array $values, array $aliases = null) use ($pdo) {
        $stmt = prepare($pdo, $sql, $values);
        if (!$stmt->execute()) {
            $error = $stmt->errorInfo();
            throw new RuntimeException(sprintf('%s, ANSI:%s', $error[2], $error[0]), $error[1]);
        }
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();

        return $row ?? null;
    };
};

$app->executeCommandDbDal = function (Application $app) {
    $pdo = $app->db1;

    return function (string $sql, array $values) use ($pdo): bool {
        $stmt = prepare($pdo, $sql, $values);
        $result = $stmt->execute();
        $error = $stmt->errorInfo();
        if (!$result && $error[1]) {
            throw new RuntimeException(sprintf('%s, ANSI:%s', $error[2], $error[0]), $error[1]);
        }
        $stmt->closeCursor();

        return $result;
    };
};
