<?php

declare(strict_types=1);

$app['db1.driver'] = 'mysql';
$app['db1.host'] = 'localhost';
$app['db1.dbname'] = '';        // database name (example: 'todo')
$app['db1.user'] = '';          // user name (example: 'root')
$app['db1.password'] = '';      // user password
$app['db1.charset'] = 'utf8';

