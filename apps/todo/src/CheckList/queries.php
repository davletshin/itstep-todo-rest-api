<?php

declare(strict_types=1);

namespace Todo\CheckList;

use Satori\Micro\Application;

$app->getListsQuery = function (Application $app) {
    $getItems = $app->getItemsDbDal;

    return function () use ($getItems): array {
        $sql = 'SELECT *
                FROM lists
                ORDER BY id DESC';

        return $getItems($sql, []);
    };
};

$app->readListQuery = function (Application $app) {
    $getItem = $app->readItemDbDal;

    return function (array $placeholders) use ($getItem) {
        $sql = 'SELECT *
                FROM lists
                WHERE id=:id';

        return $getItem($sql, $placeholders);
    };
};
