<?php

declare(strict_types=1);

namespace Todo\CheckList;

use Satori\Micro\Application;
use Satori\Middleware\Capsule;
use const Todo\JSON_CONTENT_TYPE;

$app->getListsAction = function (Application $app) {
    $getLists = $app->getListsQuery;

    return function (Capsule $capsule) use ($getLists): Capsule {
        if ($lists = $getLists()) {
            foreach ($lists as $data) {
                $data['items'] = json_decode($data['items']);
                $result[] = $data;
            }
            $capsule['success.data'] = ['success' => true, 'data' => $result];
        } else {
            $capsule['http.status'] = 404;
            $capsule['error.message'] = 'Checklists not found.';
        }

        return $capsule;
    };
};

$app->readListAction = function (Application $app) {
    $getList = $app->readListQuery;

    return function (Capsule $capsule) use ($getList): Capsule {
        $id = (int) $capsule['uri.parameters']['id'];
        $data = $getList(['id' => $id]);
        if ($data) {
            $data['items'] = json_decode($data['items']);
            $capsule['success.data'] = ['success' => true, 'data' => $data];
        } else {
            $capsule['http.status'] = 404;
            $capsule['error.message'] = sprintf('Checklist ID=%s not found.', $id);
        }

        return $capsule;
    };
};
