<?php

declare(strict_types=1);

namespace Todo;

use PDO;
use PDOException;

use Satori\Micro\Application;
use Satori\Middleware;
use Satori\Middleware\Capsule;
use Satori\Http\Response;

const JSON_CONTENT_TYPE = 'application/json; charset=utf-8';

$app = new Application();

Middleware\FastRoute\init($app, 'middleware.fast_route', ['router' => 'router']);

Middleware\Action\init($app, 'middleware.action', ['error_action' => 'errorAction']);

$app['middleware.response'] = function (\Generator $next) use ($app) {
    $capsule = yield;
    Response\sendStatusLine('1.1', $capsule['http.status']);
    Response\sendHeaders($capsule['http.headers'] + ['Content-Type' => JSON_CONTENT_TYPE]);
    if (isset($capsule['success.data']) && $capsule['success.data']) {
        Response\sendBody(json_encode($capsule['success.data']));
    }
    $next->send($capsule);

    return $next->getReturn();
};

$app->router = function (Application $app) {
    if (isset($app['router.cache_file'])) {
        return \FastRoute\cachedDispatcher($app['router.routes'], [
            'cacheFile' => $app['router.cache_file'],
            'cacheDisabled' => $app['router.cache_disabled'] ?? false,
        ]);
    }

    return \FastRoute\simpleDispatcher($app['router.routes']);
};

$app->db1 = function (Application $app) {
    $dsn = sprintf(
        "%s:host=%s;dbname=%s;charset=%s",
        $app['db1.driver'],
        $app['db1.host'],
        $app['db1.dbname'],
        $app['db1.charset']
    );
    try {
        return new PDO($dsn, $app['db1.user'], $app['db1.password'], [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ]);
    } catch (PDOException $exception) {
        Response\sendStatusLine('1.1', 500);
        Response\sendHeaders(['Content-Type' => JSON_CONTENT_TYPE]);
        Response\sendBody(json_encode([
            'success' => false,
            'message' => 'Database error.',
            'error' => [
                'exception' => $exception->getMessage(),
            ],
        ]));
        die();
    }
};

$app->errorAction = function (Application $app) {
    return function (Capsule $capsule): Capsule {
        if (!array_key_exists($capsule['http.status'] ?? 0, Response\REASON_PHRASE)) {
            $capsule['http.status'] = 500;
        }
        $status = $capsule['http.status'];
        $errorInfo['success'] = false;
        if (isset($capsule['error.message'])) {
            $errorInfo['message'] = $capsule['error.message'];
            $errorInfo['error'] = $capsule['error.details'] ?? [];
        } elseif (isset($capsule['exception'])) {
            $errorInfo['message'] = 'Thrown exception.';
            $errorInfo['error'] = [
                'exception' => $capsule['exception']->getMessage(),
            ];
        } elseif (isset(Response\REASON_PHRASE[$status])) {
            $errorInfo['message'] = 'HTTP error.';
            $errorInfo['error'] = [
                'status' => $status,
                'phrase' => Response\REASON_PHRASE[$status],
            ];
        } else {
            $errorInfo['message'] = 'Unknown error.';
        }
        $capsule['success.data'] = $errorInfo;

        return $capsule;
    };
};

$app->webEngine = function (Application $app) {
    $next = Middleware\turnBack();
    $next = $app['middleware.response']($next);
    $next = $app['middleware.action']($next);
    $pipeline = $app['middleware.fast_route']($next);

    $capsule = new Middleware\Capsule();
    $pipeline->send($capsule);
    $capsule = $pipeline->getReturn();

    return $capsule;
};
