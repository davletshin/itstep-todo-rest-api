<?php

declare(strict_types=1);

namespace Todo;

$app['router.routes'] = function (\FastRoute\RouteCollector $routes) {
    $routes->addRoute('GET', '/lists', 'getListsAction');
    $routes->addRoute('GET', '/lists/{id}', 'readListAction');
};
