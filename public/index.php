<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';
$app = \Common\Init\loadApp(__DIR__ . '/../apps/todo', 'init.json');

$capsule = $app->run('webEngine');
